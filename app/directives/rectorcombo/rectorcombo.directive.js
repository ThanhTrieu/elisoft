angular
    .module('app')
    .directive('retorCombo', rectorCombo);

function rectorCombo() {
    var directive = {
        link: linkFuc,
        templateUrl: 'directives/rectorcombo/rectorcombo.html',
        scope: {
            selectedItem: '='
        },
        restrict: 'EA',
        controller: RectorComBoController,
        controllerAs: 'vm',
        bindToController: true // because the scope is isolated
    };
    return directive;

    function linkFuc(scope, element, attrs, ctrl) {

    }
}

RectorComBoController.$inject = ['$scope', 'dataService']

function RectorComBoController($scope, dataService) {
    var vm = this;
    vm.selectItem = selectItem;

    dataService.getDataFromFile("rector.json").then(function(response) {
        vm.rectors = response.data;
    })

    function selectItem(item) {
        vm.selectedItem = item;
    }

}