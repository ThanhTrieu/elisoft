angular
    .module('app')
    .directive('copyCombo', copyCombo);

function copyCombo() {
    var directive = {
        link: linkFuc,
        templateUrl: 'directives/copycombo/copycombo.html',
        scope: {
            selectedItem: '='
        },
        restrict: 'EA',
        controller: CopyComBoController,
        controllerAs: 'vm',
        bindToController: true // because the scope is isolated
    };
    return directive;

    function linkFuc(scope, element, attrs, ctrl) {

    }
}

CopyComBoController.$inject = ['$scope', 'dataService']

function CopyComBoController($scope, dataService) {
    var vm = this;
    vm.selectedItem = {
        "id": "Rector Verso",
        "description": "Rector Verso",
        "image": "../images/recto.png"
    };
    vm.selectItem = selectItem;

    dataService.getDataFromFile("copies.json").then(function(response) {
        vm.copies = response.data;
    })

    function selectItem(item) {
        vm.selectedItem = item;
    }

}