angular
    .module('app')
    .directive('colorCombo', colorCombo);

function colorCombo() {
    var directive = {
        link: linkFuc,
        templateUrl: 'directives/colorcombo/colorcombo.html',
        scope: {
            selectedItem: '='
        },
        restrict: 'EA',
        controller: ColorComBoController,
        controllerAs: 'vm',
        bindToController: true // because the scope is isolated
    };
    return directive;

    function linkFuc(scope, element, attrs, ctrl) {

    }
}

ColorComBoController.$inject = ['$scope', 'dataService']

function ColorComBoController($scope, dataService) {
    var vm = this;
    vm.selectItem = selectItem;

    dataService.getDataFromFile("colors.json").then(function(response) {
        vm.colors = response.data;
    })

    function selectItem(item) {
        vm.selectedItem = item;
    }

}