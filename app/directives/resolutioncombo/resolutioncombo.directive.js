angular
    .module('app')
    .directive('resolutionCombo', resolutionCombo);

function resolutionCombo() {
    var directive = {
        link: linkFuc,
        templateUrl: 'directives/resolutioncombo/resolutioncombo.html',
        scope: {
            selectedItem: '='
        },
        restrict: 'EA',
        controller: resolutionComboController,
        controllerAs: 'vm',
        bindToController: true // because the scope is isolated
    };
    return directive;

    function linkFuc(scope, element, attrs, ctrl) {

    }
}

resolutionComboController.$inject = ['$scope', 'dataService']

function resolutionComboController($scope, dataService) {
    var vm = this;
    vm.selectItem = selectItem;

    dataService.getDataFromFile("resolution.json").then(function(response) {
        vm.resolutions = response.data;
    })

    function selectItem(item) {
        vm.selectedItem = item;
    }

}