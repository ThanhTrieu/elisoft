(function() {
    'use strict';

    angular.module('app.home', [])
        .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$scope', '$state', 'dataService', '$location'];

    function homeCtrl($scope, $state, dataService, $location) {
        var vm = this;
        vm.selectedColorItem = {
            "id": "Color",
            "description": "Color",
            "image": "../images/color.png"
        };
        vm.selectedRectorItem = {
            "id": "Rector Verso",
            "description": "Rector Verso",
            "image": "../images/recto.png"
        };
        vm.selectedCopyItem = {
            "id": "Aucune",
            "description": "Copies"
        };
        vm.selectedResolutionItem = {
            "id": "Normal",
            "description": "Normal 300dp",
            "image": "../images/normal.png"
        };
        vm.selectedPerson = {
            "id": "1",
            "name": "Ludovic LEROUX",
            "title": "Dossier Archive",
            "image": "../images/man.png",
            "selected": true
        };
        vm.selectPerson = selectPerson;
        vm.updateURL = updateURL;

        function initHome() {
            dataService.getDataFromFile("person.json").then(function(response) {
                vm.person = response.data;

            });

        }
        initHome();

        // $scope.$watch(angular.bind(this, function(selectedPerson) {
        //     return this.selectedPerson;
        // }), function(newVal, oldVal) {
        //     updateURL();
        // });


        function selectPerson(person) {
            person.selected = !person.selected;
            vm.selectedPerson = person;
            _.forEach(vm.person, function(p) {
                if (p.id != person.id) {
                    p.selected = false;
                }
            });
        }

        function updateURL() {
            var url = $location.path() + "?man=" + vm.selectedPerson.name + "&copies=" + vm.selectedCopyItem.id + "&webnenu1=" + vm.selectedRectorItem.id + "&webmenu2=" + vm.selectedColorItem.id + "&webmenu3=" + vm.selectedResolutionItem.id + "&filename=" + vm.nomFicher;
            $location.url(url);
        }

    }
})();