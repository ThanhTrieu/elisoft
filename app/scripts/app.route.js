(function() {
    'use strict';
    angular.module('app.route', ['ui.router'])

    .config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('home', {
                    url: '/',
                    controllerAs: 'vm',
                    controller: 'homeCtrl',
                    templateUrl: '../views/Home/home.html'
                })
        }
    ]);
})();