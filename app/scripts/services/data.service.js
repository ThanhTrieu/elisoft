(function() {
    'use strict';
    angular.module('app')
        .factory('dataService', dataService);

    dataService.$inject = ['$q', '$state', '$http'];

    function dataService($q, $state, $http) {
        var service = {
            getDataFromFile: getDataFromFile,

        }

        return service;

        /////////////

        function getDataFromFile(filename) {
            var deferred = $q.defer();
            $http.get("../resources/" + filename)
                .then(function(data, status, headers, config) {
                    deferred.resolve(data);
                }, function(data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }


        $scope.$on('$destroy', function() {

        });
    }
})();