(function() {
    'use strict';

    angular.module('app')
        .constant('_', window._)
        .run(['$rootScope', function($rootScope) {
            $rootScope._ = window._;
        }]);
})();